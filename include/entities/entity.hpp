#ifndef __ENTITIES
#define __ENTITIES

#include <iostream>
#include <string>
#include <vector>

namespace entities {
  class player {
    public:
    std::string playerName;
    std::vector<std::string> inventory;
    
    int health = 100;
    
    void         setPlayerName(std::string name) {this->playerName = name;}
    std::string  getName() {return this->playerName;}
    void         setHealth (int h) {this->health = h;}
    int          getHealth() {return this->health;}
    void         addToInventory(std::string item) {inventory.push_back(item);}
  };
};

#endif