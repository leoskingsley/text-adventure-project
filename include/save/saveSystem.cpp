#include "saveSystem.hpp"
#include <string>
#include <fstream>
#include <iostream>
#include "../modules/base64.h"
#include <sstream>
#include "../entities/entity.hpp"

void saveSystem::loadSave(saveSystem::currentSave &s, entities::player &p, std::string saveName) {
  std::fstream saveFile;
  if (saveSystem::saveExists(saveName)){
    try {
      saveFile.open("saves/" + saveName + ".dat");
    } catch (...){
      std::cout << "Failed to open save." << std::endl;
    }
    std::string sLine;
    std::getline(saveFile, sLine);
    s.setName(base64_decode(sLine));
    std::getline(saveFile, sLine);
    p.setPlayerName(base64_decode(sLine));
    std::getline(saveFile, sLine);
    //std::cout << "'" << base64_decode(sLine) << "'" << std::endl;
    p.setHealth(std::stoi(base64_decode(sLine)));
    std::getline(saveFile, sLine);
    s.setStage(std::stoi(base64_decode(sLine)));


    std::string line;
    while (std::getline(saveFile, line))
    {
      std::istringstream iss(line);
      //std::cout << base64_decode(line) << std::endl;
    }

  } else {
    std::cout << "Creating save \"" << saveName << "\"..." << std::endl;
    std::ofstream saveFile;
    try {
      saveFile.open("saves/"+saveName+".dat");
    } catch(...) {

    }
    saveFile << base64_encode(reinterpret_cast<const unsigned char*>(saveName.c_str()), saveName.length()) << std::endl;
  }
}

void saveSystem::createTestingSave() {
  std::ofstream testSave;
  try {
    testSave.open("saves/test.dat");
  } catch (...) {

  }
  std::string sName ="Testing save";
  std::string name ="Leah";
  testSave << base64_encode(reinterpret_cast<const unsigned char*>(sName.c_str()), sName.length()) << std::endl;
  testSave << base64_encode(reinterpret_cast<const unsigned char*>(name.c_str()), name.length()) << std::endl;
}

void saveSystem::createSave(saveSystem::currentSave&, std::string saveName, std::string playerName){
  std::ofstream saveFile;
  try {
    saveFile.open("saves/"+saveName+".dat");
  } catch (...) {

  }
  std::string num1 = "100";
  std::string num2 = "0";
  saveFile << base64_encode(reinterpret_cast<const unsigned char*>(saveName.c_str()), saveName.length()) << std::endl;
  saveFile << base64_encode(reinterpret_cast<const unsigned char*>(playerName.c_str()), playerName.length()) << std::endl;
  saveFile << base64_encode(reinterpret_cast<const unsigned char*>(num1.c_str()), num1.length()) << std::endl;
  saveFile << base64_encode(reinterpret_cast<const unsigned char*>(num2.c_str()), num2.length()) << std::endl;
}

void saveSystem::saveState(saveSystem::currentSave &s, std::string saveName){
  
}

bool saveSystem::saveExists(std::string saveName) {
  std::ifstream file("saves/"+saveName+".dat");
  return file.good();
}
