#include <iostream>
#include <string>
#include "../entities/entity.hpp"

namespace saveSystem {
  class currentSave {
    public:
    std::string saveName = "DEF";
    int stage = 0;

    void         setName(std::string n) {this->saveName = n;}
    std::string  getSaveName() {return this->saveName;}
    void         setStage(int s) {this->stage=s;}
    int          getStage() {return this->stage;}
  };

  void          loadSave(saveSystem::currentSave&, entities::player&, std::string saveName);
  void         saveState(saveSystem::currentSave&, std::string saveName);
  void createTestingSave();
  void        createSave(saveSystem::currentSave&, std::string saveName, std::string playerName);

  bool saveExists(std::string name);

}
