#include "utils.hpp"
#include <sys/ioctl.h>
#include <unistd.h>
#include <pthread.h>

void utils::clearScreen(){
  struct winsize window;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &window);
  for (int x = 0; x<window.ws_col; x++){
   std::cout << std::endl;
  }
}
