#include <iostream>
#include <cstring>
#include "include/entities/entity.hpp"
#include "include/save/saveSystem.hpp"
#include "include/utils/utils.hpp"
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include "include/inventoryItems/inventoryHeaders.hxx"


saveSystem::currentSave s;
entities::player p;

typedef void (*gameStageWrapper) ();

void opening() {
  std::cout << "Welcome to stage 1" << std::endl;
  std::cout << "Your health is currently " << p.getHealth() << std::endl;
  sleep(3);
  utils::clearScreen();
}

int main(){
  std::cout << "Leah Skingsley @ https://lski.xhda.cf/ 2018 <3" << std::endl;
  std::cout << "GCC 7.3.1" << std::endl;
  std::cout << std::flush;

  gameStageWrapper stages[] ={
    opening
  };


  sleep(3);
  utils::clearScreen();

  //std::cout << "Creating test save...";
  //saveSystem::createTestingSave();
  std::cout << "Please enter the name of a save to load or create: ";
  std::string inp;
  std::getline(std::cin, inp);
  std::string saveName;
  if (inp == ""){
    while(!saveSystem::saveExists(saveName)){
      std::cout << "Enter a name for the save: ";
      std::getline(std::cin, saveName);
      if (saveSystem::saveExists(saveName)) {
        std::cout << std::endl << "This save already exists!" << std::endl;
      }
    }
    std::cout << "Enter your name: ";
    std::string playerName;
    std::getline(std::cin, playerName);
    saveSystem::createSave(s, saveName, playerName);
    inp = saveName;
  }
  saveSystem::loadSave(s, p, inp);
  std::cout << std::endl << "Loaded " << s.getSaveName() << std::endl;

  std::cout << "Welcome back " << p.getName() << "!" << std::endl;
  std::cout << "Your current health is " << p.getHealth() << ". " << std::endl;
  std::cout << "You are currently at stage " << s.getStage() << std::endl;
  stages[s.getStage() - 1]();
}
